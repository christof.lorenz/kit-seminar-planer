from django.db import models
from datetime import datetime
from django.db.models.signals import post_save
from django.dispatch import receiver
import requests

class Label(models.Model):
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name
    
class Event(models.Model):
    LOCATION_CHOICES = (
        ('Large Semniar Room', 'Large Semniar Room'),
        ('Small Seminar Room', 'Small Seminar Room'),
        ('Online', 'Online'),
        ('Other', 'Other'),
    )

    LABEL_CHOICES = [
        ('PhD Seminar', 'PhD Seminar'),
        ('IFU Colloquium', 'IFU Colloquium'),
        ('Public Event', 'Public Event'),
        ('Internal Event', 'Internal Event'),

        # Add more choices as needed
    ]
        
    title = models.CharField(max_length=300)
    start_date = models.DateField(default='2024-01-01')
    end_date = models.DateField(default='2024-01-01')
    start_time = models.TimeField(default='00:00:00')
    end_time = models.TimeField(default='00:00:00')
    location = models.CharField(max_length=200, choices=LOCATION_CHOICES)
    presenter = models.CharField(max_length=200)
    contact = models.EmailField()
    label = models.CharField(max_length=200, choices=LABEL_CHOICES, default='')
    ifu_contact = models.CharField(max_length=100, default='Enter contact at IFU')
    abstract = models.TextField(max_length=1000, default='Enter a short abstract')

    def __str__(self):
        return self.title


# Define a signal handler function to create GitLab issue
@receiver(post_save, sender=Event)
def create_gitlab_issue(sender, instance, created, **kwargs):
    if created:  # Only execute when a new event is created
        # GitLab API endpoint for creating an issue
        gitlab_api_url = 'https://codebase.helmholtz.cloud/api/v4/projects/11741/issues'

        # Personal Access Token for GitLab API authentication
        gitlab_token = 'glpat-z7F_PTKhRXhcUP6PHHx1'

        tasks = [
            "Reserve Room",
            "Write Invitation Mail",
            "Write Reminder",
        # Add more tasks as needed
        ]  
        # Issue details
        issue_data = {
            'title': f'New Event: {instance.title}',
            'description': '',
            'labels': ['event']
        }

        # Add tasks to the issue description
        for index, task in enumerate(tasks, start=1):
            issue_data['description'] += f'\n{index}. [ ] {task}'  # Format each t

        # HTTP POST request to create the issue
        headers = {'PRIVATE-TOKEN': gitlab_token}
        response = requests.post(gitlab_api_url, json=issue_data, headers=headers)

        if response.status_code == 201:
            print('GitLab issue created successfully!')
        else:
            print(f'Failed to create GitLab issue. Status code: {response.status_code}, Error: {response.text}')