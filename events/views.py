from django.shortcuts import render, redirect, get_object_or_404
from .models import Event
from .forms import EventForm
from datetime import datetime
from django.http import JsonResponse

def event_list(request):
    # Get current date
    current_date = datetime.now().date()

    # Query upcoming events
    upcoming_events = Event.objects.filter(start_date__gte=current_date).order_by('start_date')[:2]
 
    # Query all events
    events = Event.objects.order_by('-start_date').all()

    context = {
        'upcoming_events': upcoming_events,
        'events': events,
        'current_date': current_date
    }

    return render(request, 'events/event_list.html', context)

def event_new(request):
    if request.method == "POST":
        form = EventForm(request.POST)
        if form.is_valid():
            event = form.save(commit=False)
            event.save()
            return redirect('event_list')
    else:
        form = EventForm()
    return render(request, 'events/event_new.html', {'form': form})
###
#def edit_event(request, event_id):
#    event = get_object_or_404(Event, pk=event_id)
#    if request.method == 'POST':
#        form = EventForm(request.POST, instance=event)
#        if form.is_valid():
#            form.save()
#            return redirect('event_list')  # Redirect to event list page after editing
#    else:
#        form = EventForm(instance=event)
#    return render(request, 'events/event_edit.html', {'form': form})
###

def edit_event(request, event_id):
    event = get_object_or_404(Event, pk=event_id)
    if request.method == 'POST':
        form = EventForm(request.POST, instance=event)
        if form.is_valid():
            form.save()
            return redirect('event_list')  # Redirect to event list page after editing
    else:
        form = EventForm(instance=event)
    return render(request, 'events/event_edit.html', {'form': form, 'event': event})

def delete_event(request, event_id):
    event = get_object_or_404(Event, pk=event_id)
    if request.method == 'POST':
        event.delete()
        return redirect('event_list')  # Redirect to event list page after deletion
    return redirect('event_list')  # Redirect if accessed through a GET request (optional)

def get_event_details(request, event_id):
    try:
        #event = Event.objects.get(id=event_id)
        event = get_object_or_404(Event, pk=event_id)
        # Assuming Event model has fields like abstract
        event_details = {
            'title': event.title,
            'presenter': event.presenter,
            'abstract': event.abstract,
            'contact': event.contact,
            'ifu_contact': event.ifu_contact,

            # Add more fields as needed
        }
        return JsonResponse(event_details)
    except Event.DoesNotExist:
        return JsonResponse({'error': 'Event not found'}, status=404)