from django.urls import path
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.contrib.auth.views import LoginView, LogoutView
from . import views

urlpatterns = [
    path('', views.event_list, name='event_list'),
    path('login/', LoginView.as_view(), name='login'),
    path('logout/', LogoutView.as_view(), name='logout'),
    path('new/', views.event_new, name='event_new'),
    path('event/new/', views.event_new, name='event_new'),
    path('event/<int:event_id>/delete/', views.delete_event, name='delete_event'),
    path('event/<int:event_id>/edit/', views.edit_event, name='edit_event'),
    path('event/<int:event_id>/', views.get_event_details, name='get_event_details'),
]

urlpatterns += staticfiles_urlpatterns()


